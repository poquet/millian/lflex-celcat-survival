# using old nixpkgs because tatsu (ics dependency) broke as of nixpkgs-22.05
{ pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/21.11.tar.gz";
    sha256 = "sha256:162dywda2dvfj1248afxc45kcrg83appjd0nmdb541hl7rnncf02";
  }) {}
, kapack ? import (fetchTarball {
    url = "https://github.com/oar-team/nur-kapack/archive/052fb35eb29228d9e4ea8afa09e9f0e390782cbd.tar.gz";
    sha256 = "sha256:0bvsgm5wv5zh3isi51sxhyryxh6g0x29id4f68c07nwvsq6qlmr9";
  }) {inherit pkgs;}
}:

let
  pyPkgs = pkgs.python3Packages;
in rec {
  lflex_celcat_survival = pyPkgs.buildPythonPackage {
    pname = "lflex_celcat_survival";
    version = "local";
    format = "pyproject";

    src = pkgs.lib.sourceByRegex ./. [
      "pyproject\.toml"
      "LICENSE"
      "lflex_celcat_survival"
      "lflex_celcat_survival/.*\.py"
      "lflex_celcat_survival/cmd"
      "lflex_celcat_survival/cmd/.*\.py"
    ];
    buildInputs = with pyPkgs; [
      flit
    ];
    propagatedBuildInputs = with pyPkgs; [
      icalendar
      pandas
      requests
      click
      beautifulsoup4
      kapack.procset
    ];
  };

  user-shell = pkgs.mkShell {
    buildInputs = with pyPkgs; [
      ipython
      lflex_celcat_survival
      ipdb
    ];
  };

  dev-shell = pkgs.mkShell {
    buildInputs = with pyPkgs; [
      ipython
    ] ++ lflex_celcat_survival.propagatedBuildInputs;
  };
}
