# lflex-celcat-survival
Set of tools to stay sane while using a CELCAT calendar at UT3.

### Rationale
As I write these lines, [the CELCAT calendar available at UT3](https://edt.univ-tlse3.fr/) cannot be used directly for teachers to know their courses. This is mainly because course time slots are not attached to teachers in the database, and many different teachers may participate in the same course. The result is that, when you want to know what courses you give next week, you end up with hundreds of time slots instead of just the deired ones.

This project tries to make CELCAT usable by:
- Fetching event data from the internal CELCAT REST API
- Filtering out all the events that do not correspond to the courses you give
- Generating an ICS with all the remaining events
- **TODO**: Providing a way to sync ics with a CalDAV server, to enable all calendar clients to synchronize from it

### Usage
```
Usage: fetch-celcat [OPTIONS] COURSE_REQUEST_FILE CREDENTIALS_FILE

Options:
  --json TEXT  If set, raw events fetched from CELCAT as JSON are written to this file.
  --csv TEXT   If set, fetched events are written as CSV to this file.
  --ics TEXT   If set, fetched events are written as ICS to this file.
  --help       Show this message and exit.
```

**The course request input file** defines which courses you are interested in.
For example, if you give the following courses:

- BAS (apogee code KINXIB11)
  - 3 TP with group INXIB11A21 for weeks 39 40 41 of academic year 2024, Tuesday at 18h
  - 3 TP with group INXIB11B31 for weeks 39 40 41 of academic year 2024, Wednesday at 10h
- SR2 (apogee code KINXIB31)
  - 3 CTD with group INXIB31B2 for weekds 42 43 48 of academic year 2024, Tuesday at 07h45

an equivalent CSV file would be the following:

```csv
mod_code,display_name,student_group,slots,duration,academic_year,weeks
KINXIB11,BAS TP,INXIB11A21,Ma18h,2h,2024-2025,39-41
KINXIB11,BAS TP,INXIB11B31,Me10h,2h,2024-2025,39-41
KINXIB31,SR2 CTD,INXIB31B2,Ma07h45,2h,2024-2025,42 43 48
```

**The credentials input file** is a JSON file that contains your UT3 credential information, for example:

```json
{
    "username": "YOUR_UT3_USERNAME",
    "password": "YOUR_UT3_PASSWORD"
}
```

### Use this script with caution
The only life jacket of this script is that it prints a warning if the number of fetched events is not the expected one for each course request.

The script works for all the courses I give as I write these lines (2024-10-07).
However, the approach is not robust so it may break when:
- There is a CELCAT server update (REST API break)
- There is a change on how CELCAT events are formatted in the UT3 database (description parsing is hardcoded and not robust)

### Installation
If you are using [Nix](https://nixos.org/) with [flakes enabled](https://nixos.wiki/wiki/Flakes#Enable_flakes), you can directly run the latest version of this code without any installation:
```sh
nix run git+https://gitlab.irit.fr/poquet/millian/lflex-celcat-survival\?ref=main#fetch-celcat -- request-file.csv credentials.json --ics /tmp/calendar.ics
```

If you are using [Nix](https://nixos.org/), you can enter a shell where the program is available by typing this command:
```sh
nix-shell https://gitlab.irit.fr/poquet/millian/lflex-celcat-survival/-/archive/main/lflex-celcat-survival-main.tar.gz -A user-shell
# then, inside the temporary shell: fetch-celcat request-file.csv credentials.json --ics /tmp/calendar.ics
```

If you are using [Nix](https://nixos.org/) but want to install this software instead:
```sh
nix-env -f https://gitlab.irit.fr/poquet/millian/lflex-celcat-survival/-/archive/main/lflex-celcat-survival-main.tar.gz -iA lflex_celcat_survival
# then, whenever you want: fetch-celcat request-file.csv credentials.json --ics /tmp/calendar.ics
```

If you are not using [Nix](https://nixos.org/), you can do a local installation via [pip](https://pip.pypa.io/en/stable/):
```sh
pip install git+https://gitlab.irit.fr/poquet/millian/lflex-celcat-survival
# then, whenever you want: fetch-celcat request-file.csv credentials.json --ics /tmp/calendar.ics
```
