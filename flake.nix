{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs?ref=24.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in rec {
        packages = import ./default.nix { inherit pkgs; };
        apps.fetch-celcat = flake-utils.lib.mkApp { drv = packages.lflex_celcat_survival; exePath = "/bin/fetch-celcat"; };
        defaultPackage = packages.lflex_celcat_survival;
      }
    );
}
