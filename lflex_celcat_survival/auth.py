import json
import requests
import urllib
from bs4 import BeautifulSoup

def parse_credentials_from_file(credentials_filename):
    with open(credentials_filename, 'r') as f:
        credentials_dict = json.load(f)
        username = credentials_dict['username']
        password = credentials_dict['password']
        teacher_code = credentials_dict['teacher_code']
    return username, password, teacher_code

def create_authenticated_session(username, password):
    s = requests.Session()
    r_headers = {"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"}

    # first simple connection. can become useful if it sets cookies
    r0 = s.get('https://edt.univ-tlse3.fr/calendar')
    assert r0.ok

    # start login procedure
    r1 = s.get('https://edt.univ-tlse3.fr/calendar/Login', allow_redirects=True)
    assert r1.ok

    soup1 = BeautifulSoup(r1.text, 'html.parser')
    assert soup1.title.string == "Service Web d'Authentification - Loading Session Information"
    forms1 = soup1.find_all('form')
    assert len(forms1) == 1, f"1 form expected in html document but {len(forms1)} were found"
    form1 = forms1[0]
    assert form1.attrs['action'] == '/idp/profile/SAML2/Redirect/SSO?execution=e2s1', f"action: {form1.attrs['action']}"
    assert form1.attrs['method'] == 'post'

    # click continue...
    r2_payload = {}
    for r1_input in form1.find_all('input'):
        if r1_input.attrs['type'] == 'hidden':
            r2_payload[r1_input.attrs['name']] = r1_input.attrs['value'] if 'value' in r1_input.attrs else ''
    r2_payload_str = urllib.parse.urlencode(r2_payload) #"&".join([f"{k}={v}" for k,v in r2_payload.items()])
    r2 = s.post('https://idp.univ-tlse3.fr/idp/profile/SAML2/Redirect/SSO?execution=e2s1',
        headers=r_headers, data=r2_payload_str, allow_redirects=True)
    assert r2.ok

    soup2 = BeautifulSoup(r2.text, 'html.parser')
    assert soup2.title.string == 'CAS - Central Authentication Service'
    forms2 = soup2.find_all('form')
    assert len(forms2) == 1, f"1 form expected in html document but {len(forms2)} were found"
    form2 = forms2[0]
    assert form2.attrs['action'] == 'login'
    assert form2.attrs['method'] == 'post'

    # give login information
    r3_payload = {}
    for r2_input in form2.find_all('input'):
        if r2_input.attrs['type'] == 'hidden':
            r3_payload[r2_input.attrs['name']] = r2_input.attrs['value'] if 'value' in r2_input.attrs else ''
    r3_payload['username'] = username
    r3_payload['password'] = password
    r3_payload_str = urllib.parse.urlencode(r3_payload) # "&".join([f"{k}={v}" for k,v in r3_payload.items()])
    r3 = s.post('https://cas.univ-tlse3.fr/cas/login', headers=r_headers, data=r3_payload_str, allow_redirects=True)
    assert r3.ok

    soup3 = BeautifulSoup(r3.text, 'html.parser')
    assert soup3.title.string == 'Informations Transmises'
    forms3 = soup3.find_all('form')
    assert len(forms3) == 1, f"1 form expected in html document but {len(forms3)} were found"
    form3 = forms3[0]
    assert form3.attrs['action'] == '/idp/profile/SAML2/Redirect/SSO?execution=e2s3'
    assert form3.attrs['method'] == 'post'

    # do not remember data forwarding consent
    r4_payload = []
    for r3_input in form3.find_all('input'):
        if r3_input.attrs['type'] == 'hidden':
            r4_payload.append(f"{r3_input.attrs['name']}={r3_input.attrs['value']}")
    r4_payload.append("_shib_idp_consentOptions=_shib_idp_doNotRememberConsent")
    r4_payload.append("_eventId_proceed=Accepter")

    r4_payload_str = "&".join(r4_payload)
    r4 = s.post('https://idp.univ-tlse3.fr/idp/profile/SAML2/Redirect/SSO?execution=e2s3',
        headers=r_headers, data=r4_payload_str, allow_redirects=True)

    soup4 = BeautifulSoup(r4.text, 'html.parser')
    forms4 = soup4.find_all('form')
    assert len(forms4) == 1, f"1 form expected in html document but {len(forms4)} were found"
    form4 = forms4[0]
    assert form4.attrs['action'] == 'https://edt.univ-tlse3.fr/calendar/Saml/AssertionConsumerService'
    assert form4.attrs['method'] == 'post'

    # click continue...
    r5_payload = {}
    for r4_input in form4.find_all('input'):
        if r4_input.attrs['type'] == 'hidden':
            r5_payload[r4_input.attrs['name']] = r4_input.attrs['value']
    r5_payload_str = urllib.parse.urlencode(r5_payload)
    r5 = s.post('https://edt.univ-tlse3.fr/calendar/Saml/AssertionConsumerService',
        headers=r_headers, data=r5_payload_str, allow_redirects=True)
    assert r5.ok

    return s
