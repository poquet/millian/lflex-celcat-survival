import logging
import requests

def do_celcat_slot_request(min_date, max_date, res_type, codes, session, url):
    assert min_date < max_date, f"min_date (value={min_date}) should be strictly greater than max_date (value={max_date})"
    headers = {"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"}
    fields = [
        f'start={min_date}',
        f'end={max_date}',
        f'resType={res_type}',
        'calView=agendaWeek',
    ] + ['federationIds%5B%5D={}'.format(code) for code in codes]
    fields_str = '&'.join(fields)

    logging.info(f'Fetching {codes} (resource type {res_type}) from {min_date} to {max_date} on url={url}')
    response = session.post(url, data=fields_str, headers=headers)
    if not response.ok:
        logging.error(f'POST HTTP request failed (status code {response.status_code}): {response.reason}')
        logging.error(f'Request response text:\n---\n{response.text}\n---')
        response.raise_for_status()
    return response.text

def do_celcat_request_subjects(min_date, max_date, subject_codes, session, url='https://edt.univ-tlse3.fr/calendar/Home/GetCalendarData'):
    '''
    Fetch calendar slots associated with specific course subjects (apogee codes) in a date range
    '''
    return do_celcat_slot_request(min_date, max_date, 100, subject_codes, session, url)

def do_celcat_request_teachers(min_date, max_date, teacher_codes, session, url='https://edt.univ-tlse3.fr/calendar/Home/GetCalendarData'):
    '''
    Fetch calendar slots associated with given teachers in a date range
    '''
    return do_celcat_slot_request(min_date, max_date, 101, teacher_codes, session, url)

def do_celcat_request_rooms(min_date, max_date, room_names, session, url='https://edt.univ-tlse3.fr/calendar/Home/GetCalendarData'):
    '''
    Fetch calendar slots associated with given rooms in a date range
    '''
    return do_celcat_slot_request(min_date, max_date, 102, room_names, session, url)

def do_celcat_request_groups(min_date, max_date, group_codes, session, url='https://edt.univ-tlse3.fr/calendar/Home/GetCalendarData'):
    '''
    Fetch calendar slots associated with given student groups in a date range
    '''
    return do_celcat_slot_request(min_date, max_date, 103, group_codes, session, url)

def do_celcat_request_students(min_date, max_date, student_codes, session, url='https://edt.univ-tlse3.fr/calendar/Home/GetCalendarData'):
    '''
    Fetch calendar slots associated with given students in a date range
    '''
    return do_celcat_slot_request(min_date, max_date, 104, student_codes, session, url)
