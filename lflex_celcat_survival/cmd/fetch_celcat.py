#!/usr/bin/env python3
import click
import logging
import lflex_celcat_survival as lcs
import pandas as pd
import sys

@click.command()
@click.argument('course_request_file')
@click.argument('credentials_file')
@click.option('--json', default=None, help='If set, raw events fetched from CELCAT as JSON are written to this file.')
@click.option('--csv', default=None, help='If set, fetched events are written as CSV to this file.')
@click.option('--ics', default=None, help='If set, fetched events are written as ICS to this file.')
def main(course_request_file, credentials_file, json, csv, ics):
    logging.basicConfig(level=logging.INFO)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.max_rows', None)
    pd.set_option('display.width', None)

    if all(o is None for o in [json, csv, ics]):
       logging.warning('No option set, doing nothing.')
       return

    username, password, teacher_code = lcs.auth.parse_credentials_from_file(credentials_file)
    session = lcs.auth.create_authenticated_session(username, password)

    requested_slots_df = lcs.slot_parse.read_weekslot_csv(course_request_file, 'fr', 2)
    celcat_slots, celcat_raw_response = lcs.events.request_slots_by_mod_code(requested_slots_df, session)

    if json is not None:
        with open(json, 'w') as f:
            f.write(celcat_raw_response)

    # slots listed in entry file but absent from celcat
    slots_not_in_celcat = celcat_slots['slot_in_celcat'].isna()
    nb_slots_not_in_celcat = slots_not_in_celcat.sum()
    if nb_slots_not_in_celcat > 0:
        logging.warning('Some defined slots are not in CELCAT!')
        print(celcat_slots[slots_not_in_celcat], file=sys.stderr)

    # slots listed in entry file and on celcat, but with no reserved room
    slots_without_reserved_rooms = celcat_slots['room_parsed'] == 'unset'
    nb_slots_without_reserved_rooms = slots_without_reserved_rooms.sum()
    if nb_slots_without_reserved_rooms > 0:
        logging.warning('Some slots are in CELCAT but there is no room reserved for them!')
        print(celcat_slots[slots_without_reserved_rooms], file=sys.stderr)

    cal_events = lcs.events.events_to_calendar_df(celcat_slots)
    if csv is not None:
        cal_events.to_csv(csv, index=False)

    calendar = lcs.events.calendar_df_to_ics(cal_events)
    if ics is not None:
        with open(ics, 'w') as f:
            f.write(calendar.to_ical().decode('utf-8'))

if __name__ == "__main__":
    main()
