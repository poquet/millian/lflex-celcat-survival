import pandas as pd
from . import fetch

class CourseRequest:
    def __init__(self, filename):
        self.df = pd.read_csv(filename, parse_dates=['begin_date', 'end_date'])
        self.df['course_request_id'] = self.df.index

    def generate_request_input(self):
        date_range_min = min(self.df['begin_date']).strftime("%Y-%m-%d")
        date_range_max = (max(self.df['end_date']) + pd.Timedelta(days=1)).strftime("%Y-%m-%d")
        apogee_codes = self.df['module_apogee'].unique()

        return (date_range_min, date_range_max, apogee_codes)

    def do_request(self, session, url='https://edt.univ-tlse3.fr/calendar/Home/GetCalendarData'):
        (date_min, date_max, apogee_codes) = self.generate_request_input()
        return fetch.do_celcat_request_subjects(date_min, date_max, apogee_codes, session, url)
